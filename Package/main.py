import sys
from Trending import Trends
from UI import UI as Ui
import matplotlib.animation as animation
from Comms import Communications
from time import sleep

def connect(port):
    if comms.tComms is not None:
        comms.tComms.stop()
        comms.tComms.join()

    if port is not None and port != "":
        comms.configure(portName=port, trend=trend)


# Handler to shut down threads before closing the application
def on_closing():
    # Wait for each thread to finish up
    if comms.tComms is not None:
        comms.tComms.stop()
        comms.tComms.join()

    # Close the window
    Ui.root.destroy()

def main(args=None):
    data_refresh_rate_ms = 33

    # Sets up the close window handler
    Ui.root.protocol("WM_DELETE_WINDOW", on_closing)

    # Assign callbacks to Ui elements.
    Ui._btnConnect.configure(command=lambda: connect(Ui._cbComms.get()))

    # Create the output trending
    # Ui.create_graphs(Ui, trend.figure)
    ani = animation.FuncAnimation(trend.figure, trend.animate, interval=data_refresh_rate_ms, blit=False)
    # Ui.update_values(Ui)


    # Gotta do this to enable Ui event generation
    # Code past mainloop is unreachable
    Ui.root.mainloop()


if __name__ == "__main__":
    comms = Communications()
    trend = Trends(Ui)
    sys.exit(main())
