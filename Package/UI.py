from tkinter import Tk, Frame, Label, Entry, LEFT, RIGHT, END, TOP, BOTH, Radiobutton, StringVar, Button, \
    Checkbutton, IntVar
from tkinter.ttk import Combobox
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import serial.tools.list_ports

class UI:
    UI_FRAME_RATE_MS = 30
    root = Tk()
    root.geometry("450x125")
    root.title("UV/IR Plotter")
    root.grid_columnconfigure(0, weight=1)

    # Create the radio button holding variables
    GraphTimeSelectedRadioSec = IntVar(root, 30)
    IsLogging = IntVar(root,0)

    # create all of the main containers
    _frComms = Frame(root, pady=3, borderwidth=3, relief="ridge")
    _frGraphOptions = Frame(root, pady=3, borderwidth=3, relief="ridge")

    _frComms.grid(row=0, column=0, sticky="")
    _frGraphOptions.grid(row=1, column=0, sticky="")

    # create the widgets for the Comms frame
    _cbComms = Combobox(_frComms, values=serial.tools.list_ports.comports(), width=50)
    _btnConnect = Button(_frComms, text="Connect")
    _lblStatus = Label(_frComms, text="", width=50)

    # Layout the widgets in the Comms frame
    _cbComms.grid(row=0, column=0)
    _btnConnect.grid(row=0, column=1)
    _lblStatus.grid(row=1,column=0, columnspan=2)

    _rdGraph1s = Radiobutton(_frGraphOptions, text='1 sec', variable=GraphTimeSelectedRadioSec, value=1)
    _rdGraph10s = Radiobutton(_frGraphOptions, text='10 sec', variable=GraphTimeSelectedRadioSec, value=10)
    _rdGraph30s = Radiobutton(_frGraphOptions, text='30 sec', variable=GraphTimeSelectedRadioSec, value=30)
    _rdGraph1m = Radiobutton(_frGraphOptions, text='1 min', variable=GraphTimeSelectedRadioSec, value=60)
    _rdGraph5m = Radiobutton(_frGraphOptions, text='5 mins', variable=GraphTimeSelectedRadioSec, value=300)

    _ckLog = Checkbutton(_frGraphOptions, text="Enable Logging", variable=IsLogging)

    # layout the widgets in the Grid Frame
    _frGraphOptions.grid_columnconfigure(7, minsize=100)
    _rdGraph1s.grid(row=0, column=0)
    _rdGraph10s.grid(row=0, column=1)
    _rdGraph30s.grid(row=0, column=2)
    _rdGraph1m.grid(row=0, column=3)
    _rdGraph5m.grid(row=0, column=4)
    _ckLog.grid(row=1, columnspan=5)





