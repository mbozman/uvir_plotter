from time import sleep, perf_counter
import serial.tools.list_ports as ports
from serial import Serial
import threading, queue
import numpy as np
import Thread
from numpy import uint8, uint16, uint32
from datetime import datetime

class Communications:

    port = None
    tComms = None
    trend = None
    serial = None
    baud = 9600
    ARDUINO_FLOAT_SIZE_BYTES = 4
    packet_size = ARDUINO_FLOAT_SIZE_BYTES * 10

    def configure(self, portName:str, trend):
        self.trend = trend
        self.port = None
        for p in ports.comports():
            if str(p) == portName:
                self.port = p
                break
        self.serial = Serial(self.port.device, self.baud, timeout=.1)

        # Spool a new thread for comms
        self.tComms = Thread.StoppableThread(target=self.comms_worker)
        self.tComms.start()

    def comms_worker(self):
        #Dump any buffered data in the port.
        while self.serial.in_waiting > 0:
            self.serial.read(self.serial.in_waiting)

        while True:
            data = None
            #check the serial port for data
            try:
                if self.serial.in_waiting > 0:
                    if self.serial.read(1) == b'$':
                        data = self.serial.read(self.packet_size)
                        timestamp = datetime.now()
                        data = np.fromstring(data, np.float32)

                        #self.trend.generate_dummy_data()
                        self.trend.append_data(timestamp, data[0], data[1],  data[2], data[3], data[4],
                                       data[5], data[6], data[7], data[8], data[9])
            except:
                ''


            # Clause for safe thread shutdown
            if self.tComms.stopped():
                    break

            sleep(0.01)
