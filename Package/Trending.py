import threading
from tkinter import IntVar
from pandas import  to_datetime, DataFrame
import matplotlib
import matplotlib.animation as animation
from time import sleep
import matplotlib.pyplot as plt
import datetime
from random import randint
import csv
import os


class Trends:
    figure = None
    df = None
    axes = None
    lock_trend = threading.Lock()
    lines = []
    csv_name = None
    last_log_status = 0

    def __init__(self, Ui):
        self.plt = plt
        self.UiReference = Ui

        self.df = DataFrame([], columns=[
            'Timestamp',
            'UV 1',
            'UV 2',
            'IR 1',
            'IR 2',
            'IR 3',
            'IR 4',
            'Vibration X',
            'Vibration Y',
            'Vibration Z',
            'Temperature'])
        self.df.Timestamp = to_datetime(self.df.Timestamp)
        self.df.set_index('Timestamp', inplace=True)

        self.figure, self.axes = self.plt.subplots(5)
        self.figure.set_size_inches(15,9)

        self.configure_axis()

        self.plt.ion()
        self.plt.show()

        # Make the directory for logging if it doesn't exist.
        if not os.path.exists("../logs"):
            os.mkdir("../logs")


    # Append data to the trend collection
    # Thread-safe
    def append_data(self,timestamp, uv1:float, uv2:float,ir1:float,ir2:float, ir3:float, ir4:float,vib_x:float, vib_y:float, vib_z:float, temp:float):
        data = {'Timestamp': [timestamp],
                'UV 1': [uv1],
                'UV 2': [uv2],
                'IR 1': [ir1],
                'IR 2': [ir2],
                'IR 3': [ir3],
                'IR 4': [ir4],
                'Vibration X': [vib_x],
                'Vibration Y': [vib_y],
                'Vibration Z': [vib_z],
                'Temperature': [temp]}

        newdf = DataFrame(data, columns=[
            'Timestamp',
            'UV 1',
            'UV 2',
            'IR 1',
            'IR 2',
            'IR 3',
            'IR 4',
            'Vibration X',
            'Vibration Y',
            'Vibration Z',
            'Temperature'])
        newdf.Timestamp = to_datetime(newdf.Timestamp)
        newdf.set_index('Timestamp', inplace=True)

        #Add the new data to the session csv file
        islogging = self.UiReference.IsLogging.get()

        self.lock_trend.acquire()

        if islogging == 1:
            if self.last_log_status == 0:
                # Create a csv for this session.
                self.csv_name = "../logs/" + str(datetime.datetime.now()) + " data.csv"
                self.csv_name = self.csv_name.replace(':', '-')
                self.df.to_csv(self.csv_name)
            try:
                newdf.to_csv(self.csv_name, mode='a', header=False)
            except:
                ''
        self.last_log_status = islogging


        self.df = self.df.append(newdf, ignore_index=False)
        # Enforce a maximum length on the data
        if self.df.last_valid_index() - self.df.index[0] > datetime.timedelta(minutes=5):
            self.df.drop(self.df.index[:1], inplace=True)
        self.lock_trend.release()

    def animate_trend(trend, kill_thread):
        sleep(1)
        while True:
            ani = animation.FuncAnimation(trend.figure, trend.animate, interval=20, blit=True)
            if kill_thread():
                break


    #Animate the chart values based off the stored dataframe info
    def animate(self, i):
        # clear the current axis
        for artist in self.plt.gca().lines + self.plt.gca().collections:
            artist.remove()

        graphTime = datetime.timedelta(seconds=self.UiReference.GraphTimeSelectedRadioSec.get())
        # make a copy of the collection to work with locally
        self.lock_trend.acquire()
        df = self.df[self.df.index > datetime.datetime.now() - graphTime]
        self.lock_trend.release()
        now = datetime.datetime.now()
        if len(df) > 2:
            if self.axes[0] is not None:
                self.axes[0].remove()
                self.axes[1].remove()
                self.axes[2].remove()
                self.axes[3].remove()
                self.axes[4].remove()
            self.axes = self.figure.subplots(5)
            self.configure_axis()
            # plot the new lines
            self.axes[0].plot(df.index, df["IR 1"], color="red", label="North", animated=True)
            self.axes[0].plot(df.index, df["IR 2"], color="lightcoral", label="South")
            self.axes[0].plot(df.index, df["IR 3"], color="orange", label="East")
            self.axes[0].plot(df.index, df["IR 4"], color="brown", label="West")

            self.axes[1].plot(df.index, df["UV 1"], color="green", label="UVA")
            self.axes[2].plot(df.index, df["UV 2"], color="lime", label="UVB")

            self.axes[3].plot(df.index, df["Vibration X"], color="blue", label="X")
            self.axes[3].plot(df.index, df["Vibration Y"], color="cyan", label="Y")
            self.axes[3].plot(df.index, df["Vibration Z"], color="black", label="Z")

            self.axes[4].plot(df.index, df["Temperature"], color="red", label="deg C")

            # Set the x axis to move with the data
            self.axes[0].set_xlim(datetime.datetime.now() - graphTime, now)
            self.axes[1].set_xlim(datetime.datetime.now() - graphTime, now)
            self.axes[2].set_xlim(datetime.datetime.now() - graphTime, now)
            self.axes[3].set_xlim(datetime.datetime.now() - graphTime, now)
            self.axes[4].set_xlim(datetime.datetime.now() - graphTime, now)

            for ax in self.axes:
                ax.legend(loc='upper right')

        return self.axes


    def configure_axis(self):
        axes = self.axes
        axes[0].xaxis.set_visible(False)
        axes[1].xaxis.set_visible(False)
        axes[2].xaxis.set_visible(False)
        axes[3].xaxis.set_visible(False)
        axes[4].xaxis.set_visible(True)
        axes[4].set_xlabel("Time")

        axes[0].set_ylabel("IR")
        axes[1].set_ylabel("UVA")
        axes[2].set_ylabel("UVB")
        axes[3].set_ylabel("Vibration")
        axes[4].set_ylabel("Temperature")


        self.plt.xticks(rotation=90)
        plt.subplots_adjust(bottom=0.18, top=1.0, left=0.05, right=0.986)

    def generate_dummy_data(self):
        self.append_data(datetime.datetime.now(),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120),
                         randint(-25, 120))
        sleep(.1)


